%include "lib.inc"
global find_word
%define offset 8 ; dq size to skip dq value
section .text

find_word:
    .loop:
	test rsi, rsi 	     ; if last element(no link) return 0
	jz .not_found
	push rsi
	add rsi, offset
	call string_equals   ; check if keys are equal
	pop rsi
	test rax, rax
	jnz .found
	mov rsi, [rsi]       ; if not equal move to the next element
	jmp .loop
    .not_found:
	xor rax, rax
	ret
    .found:
	mov rax, rsi
	ret

