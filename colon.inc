%define next_mark 0

%macro colon 2

%2: 

dq next_mark

db %1, 0

%define next_mark %2

%endmacro

