AFLAGS=-f elf64
ASM=nasm
LD=ld

all: main

.PHONY: clean

main: main.o dict.o lib.o
	$(LD) -o main $^

main.o: main.asm colon.inc words.inc dict.o
	$(ASM) $(AFLAGS) -o $@ $<

lib.o: lib.asm 
	$(ASM) $(AFLAGS) -o $@ $<

dict.o: dict.asm lib.o
	$(ASM) $(AFLAGS) -o $@ $<

clean:
	rm *.o main
