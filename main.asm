%include "lib.inc"
%include "colon.inc"
%include "words.inc"
extern find_word
%define BUFF_SIZE 255

global _start

section .rodata
error_message: db "can't read string\n", 0
not_found_message: db "key not found\n", 0

section .bss
inBuffer: resb BUFF_SIZE

section .text
_start:
    mov rdi, inBuffer
    mov rsi, BUFF_SIZE
    call read_word        ; if cant read word than error
    test rax, rax
    jz .error
    push rdx              ; saving length of word
    mov rsi, next_mark
    call find_word
    test rax, rax         ; if no key found not_found
    jz .not_found
    add rax, 8            ; if found than skip mark and key value
    mov rdi, rax
    pop rdx
    add rdi, rdx
    inc rdi
    call print_string
    call print_newline
    call exit
    .not_found:
	mov rdi, not_found_message
    jump .exit_if_error_occured
    .error:
	mov rdi, error_message
    jump .exit_if_error_occured
    .exit_if_error_occured:
	call print_error
	call exit
